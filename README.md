# SID and BRLASER 


```` 
===========
  mount -t proc proc /SID/proc  ;  mount -t sysfs sys /SID/sys ; mount -o bind /dev /SID/dev ; mount -t devpts pts /SID/dev/pts
  cp /etc/resolv.conf etc/resolv.conf ; chroot /SID 
===========
````


````
   echo init the init.d all the file and start them 
   mkdir /opt ; cd /opt
   nconfig store ppd 
   lpadmin -p HL-L2340D-series -E -v "usb://Brother/HL-L2340D%20series?serial=123212312321321321321321321321321321321321"    -P /opt/ppd-main/HL-L2340D-series.ppd
 echo "lpadmin: Printer drivers are deprecated and will stop working in a future version of CUPS."
````

````
   lpr -PHL-L2340D-series -o fit-to-page -o media=A4     "noname.pdf" 
````
 


# Laser

https://mirror.leaseweb.com/devuan/devuan_ascii/desktop-live/devuan_ascii_2.1_i386_desktop-live.iso



# HL-L2340D-series

Debian SID on raspberry PI works. 


# HP Deskjet 2700 series

It requires >= 3.21 or 3.22


https://gitlab.com/openbsd98324/debian-hplip

see: https://packages.debian.org/us/sid/armhf/hplip/download



# HP Deskjet 2721e 

Ubuntu 22.10 works fine. 
scanning and printing

https://releases.ubuntu.com/22.10/ubuntu-22.10-desktop-amd64.iso


![](medias/1673413770-screenshot.png)


# Virtualbox vm-ubuntu vmdk


Printing over Virtualbox (devuan ascii, amd64) with ubuntu

![](medias/1691145008-screenshot.png)

![](medias/1691146767-screenshot.png)

Using the scanner :

![](https://gitlab.com/openbsd98324/printer/-/raw/master/medias/1691155384-screenshot.png)





